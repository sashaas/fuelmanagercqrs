﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelManager.Core
{
	public interface ICommand
	{
	}

	public interface ICommandHandler<in TParameter> where TParameter : ICommand
	{
		Task<CommandResult> Execute(TParameter command);
	}

	public class CommandResult
	{
		public bool Success { get; set; }
		public string Message { get; set; }
		public object Data { get; set; }

		public static CommandResult CreateOk(object data)
		{
			return new CommandResult { Success = true, Data = data };
		}

		public static CommandResult CreateFailed(object data, string message = null)
		{
			return new CommandResult { Success = true, Data = data, Message = null };
		}
	}

	public interface ICommandDispatcher
	{
		Task<CommandResult> Dispatch<TParameter>(TParameter command) where TParameter : ICommand;
	}
}
