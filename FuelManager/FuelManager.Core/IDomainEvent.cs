﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelManager.Core
{
	public interface IEventStore
	{
		Task SaveChanges(string stream, int aggregareId, int version, IEnumerable<IDomainEvent> events);
		Task<IEnumerable<IDomainEvent>> GetEvents(string stream, int aggId);
	}

	public interface IDomainEvent
	{
		Guid Id { get; }

		string EventType { get; }

		object Data { get; }

		int Version { get; }
	}
}
