﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelManager.Core
{
	public interface IBus
	{
		void RegisterHandler(IEventHandler<IDomainEvent> handler);

		void RaiseEvent<TEvnt>(TEvnt evnt) where TEvnt : IDomainEvent;
	}

	public interface IEventHandler<TEvent> where TEvent : IDomainEvent
	{
		void Handle(TEvent evnt);

		bool CanHandle { get; }
	}
}
