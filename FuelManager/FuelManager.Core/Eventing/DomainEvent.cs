﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelManager.Core
{
	public class DomainEvent : IDomainEvent
	{
		public DomainEvent(Guid id, string evntType, object data, int version)
		{
			Id = id;
			EventType = evntType;
			Data = data;
			Version = version;
		}

		public Guid Id
		{
			get;
			protected set;
		}

		public string EventType
		{
			get;
			protected set;
		}

		public object Data
		{
			get;
			protected set;
		}

		public int Version
		{
			get;
			protected set;
		}
	}
}
