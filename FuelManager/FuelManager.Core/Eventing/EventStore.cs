﻿using FuelManager.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EventStore;
using EventStore.ClientAPI;
using Newtonsoft.Json;
using System.Text;
using System.Net;
using System.Threading.Tasks;

namespace FuelManager.Core
{
	public class EventStore : IEventStore
	{
		private const string EventClrTypeHeader = "EventClrTypeName";
		private const string AggregateClrTypeHeader = "AggregateClrTypeName";
		private const string CommitIdHeader = "CommitId";
		private const int WritePageSize = 500;
		private const int ReadPageSize = 500;

		private volatile bool isConnected = false;
		private readonly IEventStoreConnection _connection;

		public EventStore(IEventStoreConnection con)
		{
			if (con == null)
				throw new InvalidOperationException("EventStore connection cannot be a null.");
			_connection = con;
		}

		private async Task ConnectAsync()
		{
			await _connection.ConnectAsync();
		}

		public async Task SaveChanges(string stream, int aggregareId, int version, IEnumerable<IDomainEvent> events)
		{
			if (!isConnected)
				await ConnectAsync();

			List<EventData> storeData = new List<EventData>(events.Select(x => this.ToEventData(Guid.NewGuid(), x.Data)));
			WriteResult result = await _connection.AppendToStreamAsync(stream, 1, storeData);
		}

		public async Task<IEnumerable<IDomainEvent>> GetEvents(string stream, int aggId)
		{
			if (!isConnected)
				await ConnectAsync();

			var streamEvents = await _connection.ReadStreamEventsForwardAsync(stream, 0, 5, false);
			List<IDomainEvent> evntList = new List<IDomainEvent>(streamEvents.Events.Length);
			RecordedEvent evnt = null;
			DomainEvent domainEvnt = null;
			for (int i = 0; i < streamEvents.Events.Length; i++)
			{
				evnt = streamEvents.Events[i].Event;
				domainEvnt = new DomainEvent(evnt.EventId, evnt.EventType, JsonConvert.DeserializeObject<dynamic>(Encoding.UTF8.GetString(evnt.Data)), 1);
			}
			return evntList;
		}

		private EventData ToEventData(Guid eventId, object @event, IDictionary<string, object> headers = null)
		{
			if (headers == null) headers = new Dictionary<string, object>();
			var serializerSettings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.None };

			var data = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(@event, serializerSettings));
			var eventHeaders = new Dictionary<string, object>(headers)
			{
				{
					EventClrTypeHeader, @event.GetType().AssemblyQualifiedName
				}
			};
			var metadata = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(eventHeaders, serializerSettings));
			var typeName = @event.GetType().Name;

			return new EventData(eventId, typeName, true, data, metadata);
		}

		private object DeserializeEvent(byte[] metadata, byte[] data)
		{
			var eventClrTypeName = Newtonsoft.Json.Linq.JObject.Parse(Encoding.UTF8.GetString(metadata)).Property(EventClrTypeHeader).Value;
			return JsonConvert.DeserializeObject(Encoding.UTF8.GetString(data), Type.GetType((string)eventClrTypeName));
		}
	}
}