﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelManager.Core
{
	public class CommandDispatcher : ICommandDispatcher
	{
		private readonly IKernel _kernel;

		public CommandDispatcher(IKernel kernel)
		{
			if (kernel == null)
				throw new InvalidOperationException("Kernel cannot be a null.");
			_kernel = kernel;
		}

		public async Task<CommandResult> Dispatch<TParameter>(TParameter command) where TParameter : ICommand
		{
			var handler = _kernel.Get<ICommandHandler<TParameter>>();
			return await handler.Execute(command);
		}
	}
}
