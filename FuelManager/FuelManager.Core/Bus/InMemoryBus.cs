﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelManager.Core.Bus
{
	public class InMemoryBus : IBus
	{
		private readonly ConcurrentDictionary<Type, ConcurrentBag<IEventHandler<IDomainEvent>>> _container;

		public InMemoryBus()
		{
			_container = new ConcurrentDictionary<Type, ConcurrentBag<IEventHandler<IDomainEvent>>>();
		}

		public void RegisterHandler(IEventHandler<IDomainEvent> handler)
		{
			if (handler == null)
				throw new InvalidOperationException("Handler instance cannot be a null");

			Type type = handler.GetType();
			Type argType = type.GetGenericArguments()[0];
			ConcurrentBag<IEventHandler<IDomainEvent>> handlers;
			if (_container.TryGetValue(argType, out handlers))
				handlers.Add(handler);
			else
			{
				handlers = new ConcurrentBag<IEventHandler<IDomainEvent>>();
				handlers.Add(handler);
				_container.AddOrUpdate(type, handlers, (k, v) => handlers);
			}
		}

		public void RaiseEvent<TEvnt>(TEvnt evnt) where TEvnt : IDomainEvent
		{
			if (evnt == null)
				return;

			Type evntType = typeof(TEvnt);
			ConcurrentBag<IEventHandler<IDomainEvent>> handlers;
			if (_container.TryGetValue(evntType, out handlers))
			{
				var data = handlers.ToArray();
				for (int i = 0; i < data.Length; i++)
				{
					if (data[i].CanHandle)
						data[i].Handle(evnt);
				}
			}
		}
	}
}
