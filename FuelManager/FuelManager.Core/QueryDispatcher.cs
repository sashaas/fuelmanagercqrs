﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelManager.Core
{
	public class QueryDispatcher : IQueryDispatcher
	{
		private readonly IKernel _kernel;
		public QueryDispatcher(IKernel kernel)
		{
			if (kernel == null)
				throw new InvalidOperationException("Kernel instance cannot be a null.");

			_kernel = kernel;
		}

		public async Task<TResult> Dispatch<TParameter, TResult>(TParameter query)
			where TParameter : IQuery
			where TResult : IQueryResult
		{
			var handler = _kernel.Get<IQueryHandler<TParameter, TResult>>();
			return await handler.Retrieve(query);
		}
	}
}
