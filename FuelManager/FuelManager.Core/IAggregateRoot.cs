﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelManager.Core
{
	public interface IAggregateRoot
	{
		int Id { get; set; }

		bool CanBeSaved { get; }
	}
}
