﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelManager.Core
{
	public interface IQuery
	{
	}

	public interface IQueryResult
	{
	}

	public interface IQueryHandler<TParameter, TResult>
		where TParameter : IQuery
		where TResult : IQueryResult
	{
		Task<TResult> Retrieve(TParameter query);
	}

	public interface IQueryDispatcher
	{
		Task<TResult> Dispatch<TParameter, TResult>(TParameter query)
			where TParameter : IQuery
			where TResult : IQueryResult;
	}
}
