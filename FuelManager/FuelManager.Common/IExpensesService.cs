﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelManager.Common
{
	public interface IExpensesService
	{
		Task<List<FuelExpense>> GetPagedAsync(int userId, string sort, PagedRequest req);
		Task<List<FuelExpense>> GetAll();

		Task<FuelExpense> GetByIdAync(int id);

		Task Add(FuelExpense expense, int userId);
		Task Update(FuelExpense expense, int userId);

		Task RemoveAsync(int id);
	}
}
