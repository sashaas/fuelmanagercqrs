﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelManager.Common
{
	/// <summary>
	/// Basic implementation of UnitOfWork abstraction
	/// </summary>
	public interface IUnitOfWork : IDisposable
	{
		System.Data.Entity.DbContext Context { get; }

		IRepository<T> GetRepository<T>() where T : EntityBase;

		Task<int> SaveChanges();
	}

	public enum InstanceType
	{
		Full,
		ReadOnly
	}

	public interface IUnitOfWorkFactory
	{
		IUnitOfWork Create(InstanceType type);
	}
}
