﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FuelManager.Common
{
	public class PagedRequest
	{
		public int Page { get; set; }
		public int ItemsPerPage { get; set; }

		public int TotalCount { get; set; }
		public int TotalPages { get; set; }
	}

	public interface IRepository<T> where T : EntityBase
	{
		int Count { get; }

		IEnumerable<T> GetAll();
		Task<List<T>> GetAllAsync();
		IEnumerable<T> GetPaged(PagedRequest request);
		Task<List<T>> GetPagedAsync(PagedRequest request);
		IQueryable<T> GetBy(Expression<Func<T, bool>> exp);

		Task<T> GetById(int id);

		void Add(T entity);
		void Update(T entity);
		void Delete(T entity);
	}
}
