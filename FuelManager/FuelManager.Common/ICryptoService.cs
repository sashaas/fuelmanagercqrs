﻿using System.Threading.Tasks;

namespace FuelManager.Common
{
	public interface ICryptoService
	{
		Task<string> Encrypt(string data, string phrase);
		Task<string> Decrypt(string encrypted, string phrase);
	}
}
