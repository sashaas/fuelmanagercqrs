﻿
using System.ComponentModel.DataAnnotations;
namespace FuelManager.Common
{
	public class SummaryReport : EntityBase
	{
		public User ReportOwner { get; set; }

		[Required]
		public string Content { get; set; }
	}
}
