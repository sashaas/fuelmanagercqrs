﻿using FuelManager.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelManager.Common
{
	public class FuelExpense : EntityBase
	{
		[Required]
		public DateTime Date { get; set; }

		[Required]
		public double Litres { get; set; }

		public double PricePerLiter { get; set; }

		[Required]
		public double OverallPrice { get; set; }

		public string GasStation { get; set; }

		[Required]
		public double MileageBeforeRefuel { get; set; }

		public User Owner { get; set; }

		public int CurrencyTypeId { get; set; }
		public virtual CurrencyType Currency { get; set; }
	}
}
