﻿using FuelManager.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FuelManager.Common
{
	public class Role : EntityBase
	{
		[Required]
		[MaxLength(30)]
		public string Name { get; set; }
		public string Description { get; set; }

		public ICollection<User> Users { get; set; }
	}
}
