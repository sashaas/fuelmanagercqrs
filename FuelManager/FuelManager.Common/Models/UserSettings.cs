﻿using FuelManager.Common;

namespace FuelManager.Common
{
	public class UserSettings : EntityBase
	{
		public User Owner { get; set; }

		public int CurrencyId { get; set; }
		public CurrencyType SelctedCurrency { get; set; }
	}
}
