﻿using FuelManager.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelManager.Common
{
	public class User : EntityBase
	{
		[Required]
		public string Username { get; set; }

		[Required]
		[DataType(System.ComponentModel.DataAnnotations.DataType.EmailAddress)]
		public string Email { get; set; }

		[Required]
		[DataType(System.ComponentModel.DataAnnotations.DataType.Password)]
		public string Password { get; set; }

		public ICollection<Role> Roles { get; set; }

		public UserInfo UserInfo { get; set; }

		public UserSettings Settings { get; set; }

		public ICollection<FuelExpense> FuelExpenses { get; set; }

		public ICollection<SummaryReport> SummaryReports { get; set; }
	}
}
