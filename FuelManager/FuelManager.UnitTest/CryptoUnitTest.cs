﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FuelManager.Common;
using System.Configuration;
using System.IO;

namespace FuelManager.UnitTest
{
	[TestClass]
	public class CryptoUnitTest
	{
		private ICryptoService _service;

		[TestInitialize]
		public void Init()
		{

			_service = new CryptoService();
		}

		[TestMethod]
		public void EncryptTestMethod()
		{
			var data = _service.Encrypt("qwerty", ConfigurationManager.AppSettings["Security:Phrase"]).Result;
			Assert.IsNotNull(data);
		}


		[TestMethod]
		public void EncryptTestMethod2()
		{
			for (int i = 0; i < 1000; i++)
			{
				try
				{
					using (new BadClass(i))
					{

					}
				}
				catch (Exception)
				{
				}
			}

			using(StreamWriter sw=new StreamWriter("myfile.txt"))
			{
				sw.WriteLine("Hellloooo");
			}

			StreamReader sr = null;
			try
			{
				sr = new StreamReader("myfile.txt");
				var data = sr.ReadToEnd();
				sr.Close();
			}
			finally
			{
				sr.Close();
			}
		}
	}

	public class BadClass : IDisposable
	{
		FileStream fs;

		public BadClass(int i)
		{
			if (i % 2 == 0)
				throw new InvalidDataException("evens are bad for your health");
			fs = new FileStream(Path.GetTempFileName(), FileMode.Create);
		}

		~BadClass()
		{
			fs.Dispose();
		}

		public void Dispose()
		{
			fs.Dispose();
			GC.SuppressFinalize(this);
		}
	}
}
