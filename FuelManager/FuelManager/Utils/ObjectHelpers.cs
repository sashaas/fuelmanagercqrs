﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;

namespace FuelManager
{
	public static class ObjectHelper
	{
		public static void Set<T, TProp>(this T obj, Expression<Func<T, TProp>> property, TProp value)
		{
			if (obj == null) throw new ArgumentNullException("obj");
			if (property == null) throw new ArgumentNullException("property");

			object target = obj;
			var memberExpression = (MemberExpression)property.Body;
			var targetPropertyInfo = (PropertyInfo)memberExpression.Member;

			if (memberExpression.Expression.NodeType != ExpressionType.Parameter)
			{
				var expressions = new Stack<MemberExpression>();

				while (memberExpression.Expression.NodeType != ExpressionType.Parameter)
				{
					memberExpression = (MemberExpression)memberExpression.Expression;
					expressions.Push(memberExpression);
				}

				while (expressions.Count > 0)
				{
					var expression = expressions.Pop();
					var propertyInfo = (PropertyInfo)expression.Member;
					target = propertyInfo.GetValue(target);
					if (target == null) 
						throw new NullReferenceException(expression.ToString());
				}
			}

			// here we can additionally handle obj, target or value according to the business logic
			targetPropertyInfo.SetValue(target, value);
		}
	}
}