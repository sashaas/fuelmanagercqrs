﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace FuelManager
{
	public static class TaskUtils
	{
		public static TResult RunSync<TResult>(Func<Task<TResult>> func)
		{
			return Task.Run<Task<TResult>>(func).Unwrap().GetAwaiter().GetResult();
		}
	}
}