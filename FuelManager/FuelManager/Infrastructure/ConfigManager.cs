﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace FuelManager.Infrastructure
{
	public sealed class ConfigManager
	{
		public async Task<ConfigData> GetConfiguration()
		{
			FileInfo configF = new FileInfo(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/ConfigMap.json"));
			if (configF.Exists)
			{
				ConfigData data = await Newtonsoft.Json.JsonConvert.DeserializeObjectAsync<ConfigData>(await configF.OpenText().ReadToEndAsync());
				return data;
			}

			return null;
		}
	}


	public sealed class ConfigData
	{
		public Config Config { get; set; }
	}

	public class Config
	{
		public List<Service> Services { get; set; }
	}

	public class Service
	{
		public string Contract { get; set; }
		public string Implementation { get; set; }
		public bool Singleton { get; set; }
	}
}