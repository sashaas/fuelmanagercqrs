﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FuelManager.ViewModels
{
	public sealed class PagingHeaderVM
	{
		public int CurrentPage { get; set; }
		public int PageSize { get; set; }
		public int TotalCount { get; set; }
		public int TotalPages { get; set; }

		public string NextPage { get; set; }
		public string PreviousPage { get; set; }
	}
}