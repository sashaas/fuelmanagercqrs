﻿using System.ComponentModel.DataAnnotations;

namespace FuelManager.ViewModels
{
	public class ChangePasswordVM
	{
		[Required]
		public string OldPassword { get; set; }

		[Required]
		[DataType(DataType.Password)]
		public string Password { get; set; }
		
		[Required]
		[DataType( DataType.Password)]
		[Compare("Password")]
		public string ConfirmPassword { get; set; }
	}
}