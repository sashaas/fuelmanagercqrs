﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FuelManager.ViewModels
{
	public class SummaryViewModel
	{
		public SummaryViewModel()
		{
			Expenses = new List<ExpenseVM>();
		}

		public SummaryViewModel(List<ExpenseVM> expenses)
		{
			if (expenses == null)
				throw new InvalidOperationException("Expenses parameter cannot be a null.");
			Expenses = expenses;
		}

		public List<ExpenseVM> Expenses { get; set; }

		[Required]
		public int RefuelCount { get; set; }

		[Required]
		public double LitrespPerHundred { get; set; }

		[Required]
		public double LitresConsumedPerPeriod { get; set; }

		[Required]
		public double DistanceTraveled { get; set; }

		[Required]
		public double SpentMoney { get; set; }

		public string Message { get; set; }
	}
}