﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FuelManager.ViewModels
{
	public class ExternalLoginListViewModel
	{
		public string ReturnUrl { get; set; }
	}

	public class ExternalLoginConfirationViewModel
	{
		[Required]
		[Display(Name = "Email")]
		public string Email { get; set; }
	}
}