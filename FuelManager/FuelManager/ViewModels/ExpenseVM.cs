﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FuelManager.ViewModels
{
	public class ExpenseVM
	{
		[Required]
		public DateTime Date { get; set; }

		[Required]
		public double Litres { get; set; }

		public double PricePerLiter { get; set; }

		[Required]
		public double OverallPrice { get; set; }

		public string GasStation { get; set; }

		[Required]
		public double MileageBeforeRefuel { get; set; }
	}
}