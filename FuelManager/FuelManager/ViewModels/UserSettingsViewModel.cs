﻿
namespace FuelManager.ViewModels
{
	public class UserSettingsViewModel
	{
		public int CurrencyId { get; set; }
		public string CurrencyName { get; set; }
	}
}