﻿'use strict';

var app = angular.module('app');

app.factory('busyHttpInterceptor', ['$q', '$window', '$location', function ($q, $window, $location) {

	var busyHttpInterceptorFactory = {};

	var _getLoader = function getloader() {
		var loader = $('#divLoader');
		if (loader != undefined)
			return loader;
		else
			return undefined;
	}

	var _request = function (config) {
		_getLoader().show();
		return config;
	};
	var _response = function (response) {
		
		_getLoader().hide();
		return response;
	};
	var _responseError = function (rejection) {
		_getLoader().hide();
		return $q.reject(rejection);
	};

	busyHttpInterceptorFactory.request = _request;
	busyHttpInterceptorFactory.response = _response;
	busyHttpInterceptorFactory.responseError = _responseError;

	return busyHttpInterceptorFactory;
}]);