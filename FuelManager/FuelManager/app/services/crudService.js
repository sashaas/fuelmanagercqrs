﻿/// reference 
(function () {
	'use strict';

	var app = angular.module('app');

	var serviceId = 'crudService';

	app.factory(serviceId, function ($http, $q) {

		var self = this;

		//Get All Expenses
		self.getExpenses = function (link) {

			if(link==null)
				return $http.get("/api/fuelexpenses?page=1&pagesize=8");
			else
				return $http.get(link);
		}
		

		//Create new expense
		self.post = function (Expense) {
			var deferred = $q.defer();

			$http({
				method: "POST",
				url: "/api/fuelexpenses/create",
				data: Expense
			}).success(function (data) {
				deferred.resolve(data);
			})
			.error(function (error) {
				deferred.reject(error);
			});

			return deferred.promise;;
		}
		//Get Single Records
		self.getExpenseById = function (id) {

			var deferred = $q.defer();

			$http.get("/api/fuelexpenses/view?id=" + id)
			.success(function (data) {
				deferred.resolve(data);
			})
			.error(function (error) {
				deferred.reject(error);
			});

			return deferred.promise;
		}

		//Update the Record
		self.put = function (id, Expense) {

			var deferred = $q.defer();

			$http({
				method: "put",
				url: "/api/fuelexpenses/update?id=" + id,
				data: Expense
			})
			.success(function (data) {
				deferred.resolve(data);
			})
			.error(function (error) {
				deferred.reject(error);
			});

			return deferred.promise;
		}

		//Delete the Record
		self.delete = function (id) {
			var request = $http({
				method: "delete",
				url: "/api/fuelexpenses/delete?id=" + id
			});
			return request;
		}

		return self;
	});
})();