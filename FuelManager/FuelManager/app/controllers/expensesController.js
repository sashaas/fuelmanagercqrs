﻿(function () {
	'use strict';

	var controllerId = 'expensesController';
	var app = angular.module('app');

	//app.directive('expense-partial', function () {
	//	return {
	//		templateUrl: 'ExpensePartial.html'
	//	};
	//});

	app.controller(controllerId, ['$scope', '$http', 'crudService', 'popupService', function ($scope, $http, crudService, popupService) {

		$scope.title = "Manage your expenses here.";

		$scope.prevLink = '';
		$scope.nextLink = '';
		$scope.currentPage=0;

		$scope.expenses = [];
		$scope.isLoading = true;
		$scope.errorMsg = '';


		var promise = crudService.getExpenses(null);
		promise.then(handleSuccess,handleError);

		function handleSuccess(payload) {

			var data = payload.headers('X-Pagination');
			console.info(data);
			var objHeaders = JSON.parse(data);
			$scope.nextLink = objHeaders.nextPage;
			$scope.prevLink = objHeaders.previousPage;
			$scope.currentPage = objHeaders.currentPage;

			$scope.expenses = payload.data;
			$scope.isLoading = false;
			$scope.errorMsg = '';
		}

		function handleError(error) {
			console.error(error);
			$scope.isLoading = false;
			$scope.errorMsg = error.statusText;
			$scope.expenses = [];
		}

		$scope.delete = function (indx) {
			var rez = popupService.showPopup('Are you sure?');
			if (rez) {
				var expense = $scope.expenses[indx];
				var promise = crudService.delete(expense.id);

				promise.then(function (data) {
					$scope.expenses.splice(indx, 1); // remove 1 element starting at index [indx]
				},
				function (error) {
				});
			}
		};

		$scope.doNext = function () {
			var promise = crudService.getExpenses($scope.nextLink);
			promise.then(handleSuccess, handleError);
		};

		$scope.doPrevious = function () {
			var promise = crudService.getExpenses($scope.prevLink);
			promise.then(handleSuccess, handleError);
		};

	}]);

})();