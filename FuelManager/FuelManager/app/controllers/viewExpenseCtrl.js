﻿(function () {
	'use strict';

	var app = angular.module('app');

	var controllerId = 'viewExpenseCtrl';
	app.controller(controllerId, ['$scope', '$http', '$routeParams', 'crudService', function ($scope, $http, $routeParams, crudService) {

		$scope.title = 'View Expense.';
		$scope.isUploaded = false;

		$scope.fuelId = $routeParams.fuelId;
		console.log($scope.fuelId);

		var promise = crudService.getExpenseById($scope.fuelId);
		promise.then(
					function (payload) {
						$scope.expense = payload;
						console.log('failure loading movie', payload);
					},
					function (error) {
						$scope.isUploaded = false;
						console.log('failure loading movie', error);
					});

	}]);
})();