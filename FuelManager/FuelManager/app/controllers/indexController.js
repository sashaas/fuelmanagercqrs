﻿(function () {
	'use strict';

	var app = angular.module('app');

	app.controller('indexController', ['$scope', '$location', 'authService', function ($scope, $location, authService) {

		$scope.logOut = function () {
			authService.logOut();
			$location.path('/home');
			console.warn("Do logout");
		}

		$scope.authentication = authService.authentication;
	}]);
})();