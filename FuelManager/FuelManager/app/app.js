﻿(function () {
	'use strict';

	var app = angular.module('app', [ //Setup Angular modules 
        'ngAnimate',        // animations
        'ngRoute',          // routing,
		'ngCookies',			//cokies
		'LocalStorageModule',
		'ngResource',
        'ngSanitize',       // sanitizes html bindings (ex: sidebar.js)
	]);

	var config = {};
	app.value('config', config);

	app.config(function ($routeProvider) {

		$routeProvider.when("/home", {
			controller: "homeController",
			templateUrl: "/app/views/home.html"
		});

		$routeProvider.when("/login", {
			controller: "loginController",
			templateUrl: "/app/views/login.html"
		});

		$routeProvider.when("/signup", {
			controller: "signupController",
			templateUrl: "/app/views/signup.html"
		});

		$routeProvider.when("/expenses", {
			controller: "expensesController",
			templateUrl: "/app/views/expenses.html"
		});

		$routeProvider.when("/expense/create", {
			controller: "createExpenseCtrl",
			templateUrl: "/app/views/createExpense.html"
		});

		$routeProvider.when("/expenses/:fuelId/view/", {
			controller: "viewExpenseCtrl",
			templateUrl: "/app/views/viewExpense.html"
		});

		$routeProvider.when("/settings", {
			controller: "userSettingsCtrl",
			templateUrl: "/app/views/userSettings.html"
		});

		$routeProvider.when("/summary/view", {
			controller: "summaryViewCtrl",
			templateUrl: "/app/views/summaryView.html"
		});

		$routeProvider.when("/manageAccount", {
			controller: "manageAccountCtrl",
			templateUrl: "/app/views/manageAccount.html"
		});

		$routeProvider.otherwise({ redirectTo: "/home" });

	});

	app.config(function ($httpProvider) {
		$httpProvider.interceptors.push('authInterceptorService');
		$httpProvider.interceptors.push('busyHttpInterceptor');
	});

	app.constant('constants', {
		appName: 'FuelManager',
		appDescription: 'Tracking your fuel expenses',
		appVersion: '1.0',
		appClientId: '6B15C93B-6A1F-4A00-86A7-C2BB038DC7E3'
	});

	app.run(['$route', 'authService', function ($route, authService) {
		// Include $route to kick start the router.
		authService.fillAuthData();
	}]);

	//$locationProvider.html5Mode(true);

})();