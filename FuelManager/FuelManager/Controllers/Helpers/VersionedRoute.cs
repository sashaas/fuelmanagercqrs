﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Routing;

namespace FuelManager.Controllers.Helper
{
	internal class VersionedRouteAttribute : RouteFactoryAttribute
	{
		public VersionedRouteAttribute(string route, int allowedVersion)
			: base(route)
		{
			AllowedVersion = allowedVersion;
		}

		public int AllowedVersion
		{
			get;
			private set;
		}

		private HttpRouteValueDictionary _constraints;
		public override IDictionary<string, object> Constraints
		{
			get
			{
				if (_constraints == null)
				{
					_constraints = new HttpRouteValueDictionary();
					_constraints.Add("version", new VersioningConstraint(AllowedVersion));
				}

				return _constraints;
			}
		}
	}
}