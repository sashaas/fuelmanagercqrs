﻿using FuelManager.Common;
using FuelManager.ViewModels;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace FuelManager.Controllers.API
{
	[Authorize]
	public class SummaryController : AuthApiBase
	{
		private readonly IUnitOfWork _uow;

		public SummaryController(IUnitOfWork unity)
		{
			_uow = unity;
		}

		[HttpPost]
		[ResponseType(typeof(SummaryViewModel))]
		public async Task<IHttpActionResult> GetSummary(DateRangeRequest range)
		{
			if (ModelState.IsValid)
			{
				try
				{
					var user = await base.GetUser();
					if (user == null)
						return InternalServerError(new InvalidOperationException("No such user has been found."));

					var expenses = _uow.GetRepository<FuelExpense>().GetBy(x => x.Owner.Id == user.Id && x.Date >= range.From && x.Date <= range.To).OrderBy(x => x.Date)
						.Select(x => new ExpenseVM() { Date = x.Date, GasStation = x.GasStation, Litres = x.Litres, MileageBeforeRefuel = x.MileageBeforeRefuel, OverallPrice = x.OverallPrice, PricePerLiter = x.PricePerLiter })
						.ToList();

					if (expenses.Count <= 1)
						return Ok(new SummaryViewModel() { Message = "There is not enough data for summary report." });

					SummaryViewModel summary = new SummaryViewModel(expenses);

					summary.DistanceTraveled = expenses.Last().MileageBeforeRefuel - expenses.First().MileageBeforeRefuel;
					summary.SpentMoney = expenses.Sum(x => x.OverallPrice);
					summary.RefuelCount = expenses.Count;
					summary.LitresConsumedPerPeriod = expenses.Sum(x => x.Litres);
					summary.LitrespPerHundred = ((100 * summary.LitresConsumedPerPeriod) / summary.DistanceTraveled);

					return Ok(summary);
				}
				catch (Exception ex)
				{
					GlobalShare.Instance.Logger.Error(ex);
					return InternalServerError(ex);
				}
			}
			else
				return BadRequest();
		}


		[HttpPost]
		public async Task<IHttpActionResult> SaveSummary(SummaryViewModel summary)
		{
			if (ModelState.IsValid)
			{
				try
				{
					var user = await base.GetUser();
					if (user == null)
						return InternalServerError(new InvalidOperationException("No such user has been found."));
					int id = 0;
					string routeLink = Url.Link("RetrieveSummary", new { id = id });

					return Created<SummaryViewModel>(routeLink, null);
				}
				catch (Exception ex)
				{
					GlobalShare.Instance.Logger.Error(ex);
					return InternalServerError(ex);
				}
			}
			else
				return BadRequest();
		}

		[HttpGet]
		[ResponseType(typeof(SummaryViewModel))]
		public async Task<IHttpActionResult> RetrieveSummary(int? id)
		{
			if (ModelState.IsValid)
			{
				try
				{
					var user = await base.GetUser();
					if (user == null)
						return InternalServerError(new InvalidOperationException("No such user has been found."));

					return Ok<SummaryViewModel>(null);
				}
				catch (Exception ex)
				{
					GlobalShare.Instance.Logger.Error(ex);
					return InternalServerError(ex);
				}
			}
			else
				return BadRequest();
		}

		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
			_uow.Dispose();
		}

	}
}