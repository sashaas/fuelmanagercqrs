﻿using FuelManager.Common;
using FuelManager.Controllers.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Routing;

namespace FuelManager.Controllers.API
{
	[Authorize]
	public class FuelExpensesController : AuthApiBase
	{
		private const int _maxPageSize = 50;
		private readonly IExpensesService _service;

		public FuelExpensesController(IExpensesService service)
		{
			if (service == null)
				throw new InvalidOperationException("Service instance cannot be a null.");
			_service = service;
		}

		[VersionedRoute("api/fuelexpenses", 1, Name = "GetExpenseList")]
		[ResponseType(typeof(List<ViewModels.ExpenseVM>))]
		public async Task<IHttpActionResult> Get(string sort = "id", int page = 1, int pageSize = 10)
		{
			try
			{
				User owner = await base.GetUser();

				if (pageSize > _maxPageSize)
					pageSize = _maxPageSize;

				var pageRequest = new PagedRequest() { Page = page, ItemsPerPage = pageSize };
				var result = await _service.GetPagedAsync(owner.Id, sort, pageRequest);

				var urlHelper = new UrlHelper(Request);
				var prevLink = page > 1 ? urlHelper.Link("GetExpenseList", new { sort = sort, page = page - 1, pageSize = pageSize }) : "";
				var nextLink = page < pageRequest.TotalPages ? urlHelper.Link("GetExpenseList", new { sort = sort, page = page + 1, pageSize = pageSize }) : "";

				var header = new ViewModels.PagingHeaderVM()
				{
					CurrentPage = page,
					PageSize = pageSize,
					TotalCount = pageRequest.TotalCount,
					TotalPages = pageRequest.TotalPages,
					NextPage = nextLink,
					PreviousPage = prevLink
				};

				var set = new Newtonsoft.Json.JsonSerializerSettings()
				{
					ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver()
				};

				System.Web.HttpContext.Current.Response.Headers.Add("X-Pagination",
					await Newtonsoft.Json.JsonConvert.SerializeObjectAsync(header, Newtonsoft.Json.Formatting.None, set));

				return Ok(result.Select(x => AutoMapper.Mapper.Map<ViewModels.ExpenseVM>(x)));
			}
			catch (Exception ex)
			{
				GlobalShare.Instance.Logger.Error(ex);
				return InternalServerError(ex);
			}
		}

		// GET api/<controller>/view/5
		[HttpGet]
		[ResponseType(typeof(FuelExpense))]
		public async Task<IHttpActionResult> View(int id)
		{
			try
			{
				var expense = await _service.GetByIdAync(id);
				return Ok(expense);
			}
			catch (Exception ex)
			{
				GlobalShare.Instance.Logger.Error(ex.Message, ex);
				return InternalServerError(ex);
			}
		}

		[HttpPost]
		[ResponseType(typeof(FuelExpense))]
		public async Task<IHttpActionResult> Create(FuelExpense expense)
		{
			if (expense == null)
				return BadRequest();

			try
			{
				User user = await GetUser();
				var t = _service.Add(expense, user.Id);
				await t;
			}
			catch (InvalidOperationException ex)
			{
				GlobalShare.Instance.Logger.Warn(ex.Message);
				return BadRequest(ex.Message);
			}
			catch (Exception ex)
			{
				GlobalShare.Instance.Logger.Error(ex.Message, ex);
				return InternalServerError(ex);
			}

			return Ok<FuelExpense>(expense);
		}

		[HttpDelete]
		public async Task<IHttpActionResult> Delete(int id)
		{
			try
			{
				await _service.RemoveAsync(id);
				return Ok();
			}
			catch (InvalidOperationException ex)
			{
				GlobalShare.Instance.Logger.Warn(ex.Message);
				return BadRequest(ex.Message);
			}
			catch (Exception ex)
			{
				GlobalShare.Instance.Logger.Error(ex.Message, ex);
				return InternalServerError();
			}
		}
	}
}