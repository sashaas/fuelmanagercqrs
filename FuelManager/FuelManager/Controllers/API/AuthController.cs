﻿using FuelManager.Common;
using FuelManager.Common.Models;
using FuelManager.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace FuelManager.Controllers.API
{
	//[RoutePrefix("api/auth")]
	public class AuthController : AuthApiBase
	{
		private static readonly string secretKey = System.Configuration.ConfigurationManager.AppSettings["Security:Key"];

		static AuthController() { }

		private readonly IUserService _userSvc;
		private readonly IAuthService _authSvc;

		public AuthController(IUserService u, IAuthService a)
		{
			_userSvc = u;
			_authSvc = a;
		}

		[HttpPost]
		[AllowAnonymous]
		public async Task<IHttpActionResult> Login(LoginModel loginData)
		{
			if (!ModelState.IsValid)
				return BadRequest(ModelState);

			try
			{
				AuthResponse resp = await _authSvc.AuthenticateUser(loginData.Username, loginData.Password);
				if (resp.Status != AuthStatus.Ok)
				{
					if (resp.Status != AuthStatus.Ok)
					{
						ModelState.Clear();
						if (resp.Status == AuthStatus.InvalidPassword)
							ModelState.AddModelError("password", "Invalid password");
						else if (resp.Status == AuthStatus.UserNotFound)
							ModelState.AddModelError("username", "User has not been found.");
						else if (resp.Status == AuthStatus.Error)
							return InternalServerError(resp.Error);

						return BadRequest(RetrieveModelState());
					}
				}

				string role = await _userSvc.GetUserRole(loginData.Username);

				IDictionary<string, object> data = new Dictionary<string, object>(5);
				data.Add("iss", loginData.Username);
				data.Add("aud", role);
				data.Add("iat", (int)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
				data.Add("exp", (int)DateTime.UtcNow.AddHours(1).Subtract(new DateTime(1970, 1, 1)).TotalSeconds);

				//http://mikehadlow.blogspot.com/2014/04/json-web-tokens-owin-and-angularjs.html
				//https://github.com/johnsheehan/jwt/tree/master/JWT
				//http://bitoftech.net/2014/10/27/json-web-token-asp-net-web-api-2-jwt-owin-authorization-server/
				//http://www.jayway.com/2014/09/25/securing-asp-net-web-api-endpoints-using-owin-oauth-2-0-and-claims/

				var token = JsonWebToken.Encode(data, secretKey, JwtHashAlgorithm.HS256);
				return Ok(new JwtTokenResponse { Token = token });
			}
			catch (System.Exception ex)
			{
				GlobalShare.Instance.Logger.Error(ex);
				return InternalServerError(ex);
			}
		}

		private async Task SignInAsync(ClaimsIdentity userId, bool isPersistent)
		{
			AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
			AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, userId);
			await Task.FromResult<object>(null);
		}


		[HttpPost]
		[Authorize]
		public async Task<IHttpActionResult> Logout()
		{
			Claim claim = AuthenticationManager.User.FindFirst(ClaimTypes.Name);
			string role = await _userSvc.GetUserRole(claim.Value);
			if (string.IsNullOrEmpty(role))
				return BadRequest("Invalid user name.");
			await _authSvc.Logout(claim.Value);
			AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
			return Ok();
		}

		// POST api/Account/Register
		[HttpPost]
		[AllowAnonymous]
		public async Task<IHttpActionResult> Register(RegisterViewModel model)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			IHttpActionResult errorResult = await GetErrorResult(model);

			if (errorResult != null)
			{
				return errorResult;
			}

			return Ok();
		}

		private async Task<IHttpActionResult> GetErrorResult(RegisterViewModel model)
		{
			if (model == null || model.ConfirmPassword != model.Password)
				return BadRequest();

			var result = await _authSvc.Register(model.Username, model.Password, model.Email);

			if (result.Status != AuthStatus.Ok)
			{
				if (result.Status == AuthStatus.InvalidPassword)
					ModelState.AddModelError("", "Invalid password");
				else if (result.Status == AuthStatus.UserWithSuchNameExists)
					ModelState.AddModelError("", "User with such name already exists.");
				else if (result.Status == AuthStatus.Error)
					return InternalServerError(result.Error);

				return BadRequest(RetrieveModelState());
			}
			return null;
		}


		[HttpPost]
		[Authorize]
		public async Task<IHttpActionResult> ChangePassword(ChangePasswordVM change)
		{
			if (ModelState.IsValid)
			{
				if (change.Password != change.ConfirmPassword)
					return BadRequest("new password and confirm password must be the same.");

				try
				{
					User usr = await base.GetUser();
					await _userSvc.ChangeUserPassword(usr.Username, change.OldPassword, change.Password);
				}
				catch (System.Exception ex)
				{
					GlobalShare.Instance.Logger.Error(ex);
					return InternalServerError(ex);
				}
				return Ok();
			}
			else
			{
				return BadRequest(RetrieveModelState());
			}
		}

		private string RetrieveModelState()
		{
			StringBuilder sb = new StringBuilder();
			foreach (var item in ModelState.Values)
			{
				foreach (var subItem in item.Errors)
				{
					sb.AppendLine(subItem.ErrorMessage);
				}
			}
			return sb.ToString();
		}
	}
}