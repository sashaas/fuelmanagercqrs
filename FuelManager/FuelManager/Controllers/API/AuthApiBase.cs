﻿using FuelManager.Common;
using Microsoft.Owin.Security;
using System.Data.Entity;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;

namespace FuelManager.Controllers.API
{
	public class AuthApiBase : ApiController
	{
		protected IAuthenticationManager AuthenticationManager
		{
			get { return this.Request.GetOwinContext().Authentication; }
		}
		
		[Ninject.Inject]
		public IUnitOfWorkFactory Factory { get; set; }

		protected async Task<User> GetUser()
		{
			var owinUser = AuthenticationManager.User;
			Claim nameClaim = owinUser.FindFirst(ClaimTypes.NameIdentifier);
			using (IUnitOfWork uow = Factory.Create(InstanceType.ReadOnly))
			{
				var repo = uow.GetRepository<User>();
				var user = await repo.GetBy(x => x.Username == nameClaim.Value).FirstOrDefaultAsync();
				return user;
			}
		}
	}
}