﻿using FuelManager.Common;
using FuelManager.Core;
using System;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace FuelManager.Services
{
	public class UserService : IUserService
	{
		private readonly IQueryDispatcher _queryDisp;
		private readonly ICommandDispatcher _comDisp;

		public UserService(IQueryDispatcher disp, ICommandDispatcher comDisp)
		{
			_queryDisp = disp;
			_comDisp = comDisp;
		}

		public async Task<string> GetUserRole(string username)
		{
			if (string.IsNullOrEmpty(username))
				throw new InvalidOperationException("User name cannot be empty.");

			var result = await _queryDisp.Dispatch<CQRS.Query.GetUserRolesQuery, CQRS.Query.GetUserRolesResult>(new CQRS.Query.GetUserRolesQuery(username));
			return string.Join(",", result.Roles);
		}

		public async Task<UserSettings> GetSettings(string username)
		{
			var result = await _queryDisp.Dispatch<CQRS.Query.GetUserSettingsQuery, CQRS.Query.GetUserSettingsResult>(new CQRS.Query.GetUserSettingsQuery(username));
			return result.Settings;
		}

		public async Task UpdateSettingsCurrency(string username, CurrencyType newCurrency)
		{
			if (newCurrency == null)
				throw new InvalidOperationException("New currency cannot be a null");
			if (string.IsNullOrEmpty(username))
				throw new InvalidOperationException("User name cannot be empty.");

			var result = await _comDisp.Dispatch<CQRS.Command.UpdateUserCurrencyCommand>(new CQRS.Command.UpdateUserCurrencyCommand(username, newCurrency.Id));
			if (!result.Success)
				throw new InvalidOperationException("Command wasn't executed.");
		}

		public async Task ChangeUserPassword(string uname, string oldPassword, string newPassword)
		{
			if (string.IsNullOrEmpty(uname))
				throw new InvalidOperationException("User name cannot be empty.");
			if (string.IsNullOrEmpty(oldPassword))
				throw new InvalidOperationException("Old password cannot be empty.");
			if (string.IsNullOrEmpty(newPassword))
				throw new InvalidOperationException("New password cannot be empty.");

			var result = await _comDisp.Dispatch<CQRS.Command.ChangeUserPasswordCommand>(new CQRS.Command.ChangeUserPasswordCommand(uname, oldPassword, newPassword));
			if (!result.Success)
				throw new InvalidOperationException("Command execution was failed.");
		}
	}
}