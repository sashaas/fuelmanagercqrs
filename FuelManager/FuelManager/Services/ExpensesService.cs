﻿using FuelManager.Common;
using FuelManager.Controllers.Helpers;
using FuelManager.Core;
using FuelManager.CQRS.Query;
using FuelManager.Query;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace FuelManager.Services
{
	public class ExpensesService : IExpensesService
	{
		private readonly IUnitOfWorkFactory _factory;
		private readonly IQueryDispatcher _queryDispatcher;
		private readonly ICommandDispatcher _commandDispatcher;

		public ExpensesService(IUnitOfWorkFactory fact, IQueryDispatcher disp, ICommandDispatcher comDisp)
		{
			if (disp == null)
				throw new InvalidOperationException("Query dispatcher instance cannot be a null.");
			if (comDisp == null)
				throw new InvalidOperationException("Command dispatcher instance cannot be a null.");

			_queryDispatcher = disp;
			_commandDispatcher = comDisp;
			_factory = fact;
		}

		public async Task<List<Common.FuelExpense>> GetPagedAsync(int ownerId, string sort, PagedRequest request)
		{
			ShowPagedQuery cqrsQuery = new ShowPagedQuery(request.ItemsPerPage, request.Page, ownerId, sort);
			var result = await _queryDispatcher.Dispatch<ShowPagedQuery, ShowPagedResult<FuelExpense>>(cqrsQuery);
			return result.Result;
		}

		public async Task<List<Common.FuelExpense>> GetAll()
		{
			using (IUnitOfWork uow = _factory.Create(InstanceType.ReadOnly))
			{
				var repo = uow.GetRepository<FuelExpense>();
				var data = repo.GetAll().ToList();
				return await Task.FromResult<List<Common.FuelExpense>>(data);
			}
		}

		public async Task<Common.FuelExpense> GetByIdAync(int id)
		{
			GetByIdResult<FuelExpense> result = await _queryDispatcher.Dispatch<GetByIdQuery, GetByIdResult<FuelExpense>>(new CQRS.Query.GetByIdQuery(id));
			return result.Data;
		}

		public async Task Add(Common.FuelExpense expense, int userId)
		{
			var cmd = new FuelManager.CQRS.Command.CreateNewExpenseCommand(userId, expense.Date, expense.Litres, expense.OverallPrice,
				expense.MileageBeforeRefuel, expense.GasStation, expense.PricePerLiter);
			var result = await _commandDispatcher.Dispatch<FuelManager.CQRS.Command.CreateNewExpenseCommand>(cmd);
			if (result.Success)
				GlobalShare.Instance.Logger.Info("Command has been processed.");
		}

		public async Task RemoveAsync(int id)
		{
			var t = _commandDispatcher.Dispatch<CQRS.Command.DeleteExpenseCommand>(new CQRS.Command.DeleteExpenseCommand(id));
			await t;
		}

		public Task Update(Common.FuelExpense expense, int userId)
		{
			throw new NotImplementedException();
		}
	}
}