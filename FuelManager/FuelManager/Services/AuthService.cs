﻿using FuelManager.Common;
using FuelManager.DataAccess;
using System.Threading.Tasks;
using System.Linq;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Configuration;
namespace FuelManager.Services
{
	public class AuthService : IAuthService
	{
		private readonly IUnitOfWorkFactory _fact;
		private readonly Core.ICommandDispatcher _disp;

		public AuthService(IUnitOfWorkFactory factory, Core.ICommandDispatcher disp)
		{
			_fact = factory;
			_disp = disp;
		}

		public async Task<AuthResponse> AuthenticateUser(string name, string pass)
		{
			if (string.IsNullOrEmpty(name))
				throw new InvalidOperationException("User name cannot be empty.");
			if (string.IsNullOrEmpty(pass))
				throw new InvalidOperationException("User password cannot be empty.");
			var result = await _disp.Dispatch<CQRS.Command.AuthenticateUserCommand>(new CQRS.Command.AuthenticateUserCommand(name, pass));
			return result.Data as AuthResponse;
		}

		public async Task<AuthResponse> Logout(string uname)
		{
			using (IUnitOfWork uow = _fact.Create(InstanceType.Full))
			{
				IRepository<User> userRepo = uow.GetRepository<User>();
				IRepository<UserInfo> userInfoRepo = uow.GetRepository<UserInfo>();

				var user = await userRepo.GetBy(x => x.Username == uname).Include("UserInfo").FirstOrDefaultAsync();
				if (user != null)
				{
					user.UserInfo.LastAccess = DateTime.Now;
				}

				userInfoRepo.Update(user.UserInfo);
				await uow.SaveChanges();
			}

			return AuthResponse.CreateOk();
		}

		public async Task<AuthResponse> Register(string name, string password, string email)
		{
			if (string.IsNullOrEmpty(name))
				throw new InvalidOperationException("User name cannot be empty.");
			if (string.IsNullOrEmpty(password))
				throw new InvalidOperationException("User password cannot be empty.");

			var result = await _disp.Dispatch<CQRS.Command.RegisterUserCommand>(new CQRS.Command.RegisterUserCommand(name, password, email));
			return result.Data as AuthResponse;
		}
	}
}