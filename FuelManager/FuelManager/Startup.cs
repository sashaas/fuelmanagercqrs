﻿using Owin;
using System.Web.Http;
using Hangfire;
using Hangfire.SqlServer;
using System;
using Newtonsoft.Json.Converters;
using FuelManager.Common;

[assembly: Microsoft.Owin.OwinStartup(typeof(FuelManager.Startup))]
namespace FuelManager
{
	public partial class Startup
	{
		public void Configuration(IAppBuilder builder)
		{
			InitMappings();

			ConfigureAuthentication(builder);
			builder.Use<Middlewares.RequestLoggerMiddleware>(GlobalShare.Instance.Logger);
			BuildWebApi(builder);

			builder.UseHangfire(config =>
			{
				config.UseNinjectActivator(GlobalShare.Instance.Kernel);

				// Basic setup required to process background jobs.
				var options = new SqlServerStorageOptions
				{
					InvisibilityTimeout = TimeSpan.FromMinutes(30), //default value
				};

				//config.UseFilter(new Infrastructure.Attributes.LogFailureAttribute());
				config.UseSqlServerStorage("FuelCtx", options);
				config.UseServer();
			});
		}

		public void BuildWebApi(IAppBuilder builder)
		{
			HttpConfiguration config = new HttpConfiguration();



			config.Routes.MapHttpRoute("DefaultActionApi", "api/{controller}/{action}/{id}", new { id = RouteParameter.Optional });
			config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}", new { id = RouteParameter.Optional });
			//config.Routes.IgnoreRoute("App", "app/{*params}"); the same as below
			config.Routes.MapHttpRoute("App", "app/{*params}", null, null, new System.Web.Http.Routing.StopRoutingHandler());


			config.DependencyResolver = new Infrastructure.WebApiNinjectDependencyResolver(GlobalShare.Instance.Kernel);
			config.Formatters.Remove(config.Formatters.XmlFormatter);
			config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver();
			//config.Formatters.JsonFormatter.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-ddTHH:mmZ" });
			config.Formatters.JsonFormatter.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.IsoDateTimeConverter());

			//Install-Package Microsoft.Owin.Cors -Version 2.1.0
			//builder.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

			builder.UseWebApi(config);
		}

		public void InitMappings()
		{
			AutoMapper.Mapper.CreateMap<FuelExpense, ViewModels.ExpenseVM>();
			AutoMapper.Mapper.CreateMap<CQRS.Command.CreateNewExpenseCommand, FuelExpense>();
			AutoMapper.Mapper.CreateMap<Common.CurrencyType, ViewModels.CurrencyViewModel>();
			AutoMapper.Mapper.CreateMap<Common.UserSettings, ViewModels.UserSettingsViewModel>();
		}
	}
}