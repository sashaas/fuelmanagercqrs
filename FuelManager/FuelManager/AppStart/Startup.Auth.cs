﻿using FuelManager.Middlewares.Auth;
using Owin;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace FuelManager
{
	public partial class Startup
	{
		public void ConfigureAuthentication(IAppBuilder app)
		{
			Middlewares.Auth.BearerAuthOptions opt = new Middlewares.Auth.BearerAuthOptions("FuelManager", ValidateToken);
			app.UseBearerAuthentication(opt);
		}

		private async Task<IEnumerable<Claim>> ValidateToken(string token)
		{
			try
			{
				var key = System.Configuration.ConfigurationManager.AppSettings["Security:Key"];
				var payload = await JsonWebToken.DecodeToObject<IDictionary<string, object>>(token, key);

				int current = (int)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
				if ((Int64)payload["exp"] < current)
					return null;

				List<Claim> claims = new List<Claim>(3);
				claims.AddRange(new List<Claim>() { new Claim(ClaimTypes.NameIdentifier, payload["iss"] as string), new Claim(ClaimTypes.Role, payload["aud"] as string) ,
				new Claim(ClaimTypes.Name, payload["iss"]as string)});
				return claims;
			}
			catch (SignatureVerificationException ex)
			{
				GlobalShare.Instance.Logger.Error(ex);
				return null;
			}
		}
	}
}