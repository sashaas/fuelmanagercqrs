﻿using log4net;
using Ninject;
using Ninject.Extensions.Conventions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;

namespace FuelManager
{
	public class GlobalShare
	{
		private static Lazy<GlobalShare> _instance = new Lazy<GlobalShare>(() => new GlobalShare());

		private GlobalShare()
		{
			log4net.Config.XmlConfigurator.Configure();
			Logger = LogManager.GetLogger(typeof(GlobalShare));

			Kernel = CreateKernel().Result;
		}

		public static GlobalShare Instance
		{
			get
			{
				return _instance.Value;
			}
		}

		public StandardKernel Kernel { get; set; }
		public ILog Logger { get; set; }

		public async Task<StandardKernel> CreateKernel()
		{
			var kernel = new StandardKernel();
			kernel.Load(System.Reflection.Assembly.GetExecutingAssembly());
			kernel.Load("FuelManager.Common");
			kernel.Load("FuelManager.Core");

			kernel.Bind(x => x.FromAssembliesMatching("FuelManager.dll").
							SelectAllClasses().InheritedFrom(typeof(FuelManager.Core.IQueryHandler<,>)).BindAllInterfaces());
			kernel.Bind(x => x.FromAssembliesMatching("FuelManager.dll").
							SelectAllClasses().InheritedFrom(typeof(FuelManager.Core.ICommandHandler<>)).BindAllInterfaces());

			Infrastructure.ConfigManager confMan = new Infrastructure.ConfigManager();
			Infrastructure.ConfigData data = await confMan.GetConfiguration();
			if (data != null && data.Config.Services != null && data.Config.Services.Count > 0)
			{
				foreach (var service in data.Config.Services)
				{
					if (!service.Singleton)
						kernel.Bind(System.Type.GetType(service.Contract)).To(System.Type.GetType(service.Implementation));
					else
						kernel.Bind(System.Type.GetType(service.Contract)).To(System.Type.GetType(service.Implementation)).InSingletonScope();
				}
			}

			kernel.Bind<ILog>().ToMethod((ctx) => GlobalShare.Instance.Logger);
			//kernel.Bind<ILog>().ToMethod((ctx) => GlobalShare.Instance.Kernel);
			kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => GlobalShare.Instance.Kernel);
			kernel.Bind<EventStore.ClientAPI.IEventStoreConnection>().
				ToMethod(ctx => EventStore.ClientAPI.EventStoreConnection.Create(new IPEndPoint(IPAddress.Loopback, 2113)));

			return kernel;
		}
	}
}