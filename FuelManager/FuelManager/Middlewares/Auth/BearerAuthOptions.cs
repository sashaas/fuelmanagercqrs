﻿using Microsoft.Owin.Security;

namespace FuelManager.Middlewares.Auth
{
	public class BearerAuthOptions : AuthenticationOptions
	{
		public BearerAuthMiddleware.CredentialValidationFunction CredentialValidationFunction { get; private set; }
		public string Realm { get; private set; }

		public BearerAuthOptions(string realm, BearerAuthMiddleware.CredentialValidationFunction validationFunction)
			: base("Bearer")
		{
			Realm = realm;
			CredentialValidationFunction = validationFunction;
		}
	}
}