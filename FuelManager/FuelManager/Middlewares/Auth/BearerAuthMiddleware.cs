﻿using Microsoft.Owin;
using Microsoft.Owin.Security.Infrastructure;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace FuelManager.Middlewares.Auth
{
	public class BearerAuthMiddleware : AuthenticationMiddleware<BearerAuthOptions>
	{
		public delegate Task<IEnumerable<Claim>> CredentialValidationFunction(string token);

		public BearerAuthMiddleware(OwinMiddleware next, BearerAuthOptions options)
			: base(next, options)
		{ }

		protected override AuthenticationHandler<BearerAuthOptions> CreateHandler()
		{
			return new BasicAuthenticationHandler(Options);
		}
	}
}