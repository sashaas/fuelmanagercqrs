﻿using Owin;

namespace FuelManager.Middlewares.Auth
{
	public static class BearerAuthExtensions
	{
		public static IAppBuilder UseBearerAuthentication(this IAppBuilder app, string realm, BearerAuthMiddleware.CredentialValidationFunction validationFunction)
		{
			var options = new BearerAuthOptions(realm, validationFunction);
			return app.UseBearerAuthentication(options);
		}

		public static IAppBuilder UseBearerAuthentication(this IAppBuilder app, BearerAuthOptions options)
		{
			return app.Use<BearerAuthMiddleware>(options);
		}
	}
}