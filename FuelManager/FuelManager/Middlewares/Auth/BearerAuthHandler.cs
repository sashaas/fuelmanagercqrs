﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Infrastructure;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace FuelManager.Middlewares.Auth
{
	class BasicAuthenticationHandler : AuthenticationHandler<BearerAuthOptions>
	{
		private readonly string _challenge;

		public BasicAuthenticationHandler(BearerAuthOptions options)
		{
			_challenge = "Bearer realm=" + options.Realm;
		}

		protected override async Task<AuthenticationTicket> AuthenticateCoreAsync()
		{
			var authzValue = Request.Headers.Get("Authorization");
			if (string.IsNullOrEmpty(authzValue) || !authzValue.StartsWith("Bearer ", StringComparison.OrdinalIgnoreCase))
				return null;

			var token = authzValue.Substring("Bearer ".Length).Trim();
			var claims = await TryGetPrincipalFromBasicCredentials(token, Options.CredentialValidationFunction);

			if (claims == null)
				return null;

			var id = new ClaimsIdentity(claims, Options.AuthenticationType);
			return new AuthenticationTicket(id, new AuthenticationProperties());
		}

		protected override Task ApplyResponseChallengeAsync()
		{
			if (Response.StatusCode == 401)
			{
				var challenge = Helper.LookupChallenge(Options.AuthenticationType, Options.AuthenticationMode);
				if (challenge != null)
				{
					Response.Headers.AppendValues("WWW-Authenticate", _challenge);
				}
			}

			return Task.FromResult<object>(null);
		}

		async Task<IEnumerable<Claim>> TryGetPrincipalFromBasicCredentials(string token, BearerAuthMiddleware.CredentialValidationFunction validate)
		{
			return await validate(token);
		}
	}
}