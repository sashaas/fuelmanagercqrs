﻿using FuelManager.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FuelManager.CQRS.Command
{
	public class UpdateUserCurrencyCommand : ICommand
	{
		public UpdateUserCurrencyCommand(string username, int newCurId)
		{
			UserName = username;
			NewCurrencyId = newCurId;
		}

		public string UserName { get; private set; }
		public int NewCurrencyId { get; private set; }
	}
}