﻿using FuelManager.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FuelManager.CQRS.Command
{
	public class ChangeUserPasswordCommand : ICommand
	{
		public ChangeUserPasswordCommand(string user, string old, string newPass)
		{
			UserName = user;
			OldPassword = old; NewPassword = newPass;
		}

		public string UserName { get; private set; }
		public string OldPassword { get; private set; }
		public string NewPassword { get; private set; }
	}
}