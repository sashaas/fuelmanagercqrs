﻿using FuelManager.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FuelManager.CQRS.Command
{
	public class CreateNewExpenseCommand : ICommand, IAggregateRoot
	{
		public CreateNewExpenseCommand(int userId, DateTime time, double litres,
			double price, double mileage, string gasStation = null, double pricePerLiter = 0.0)
		{
			UserId = userId;
			Date = time;
			Litres = litres;
			OverallPrice = price;
			MileageBeforeRefuel = mileage;
			GasStation = gasStation;
			PricePerLiter = pricePerLiter;
		}

		public int UserId { get; private set; }

		public DateTime Date { get; private set; }

		public double Litres { get; private set; }

		public double PricePerLiter { get; private set; }

		public double OverallPrice { get; private set; }

		public string GasStation { get; private set; }

		public double MileageBeforeRefuel { get; private set; }

		public int Id
		{
			get { return 0; }
			set { }
		}

		public bool CanBeSaved
		{
			get
			{
				return Date != new DateTime() && Litres != 0.0 && OverallPrice != 0 && MileageBeforeRefuel != 0;
			}
		}
	}
}