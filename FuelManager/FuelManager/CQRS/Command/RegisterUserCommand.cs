﻿using FuelManager.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FuelManager.CQRS.Command
{
	public class RegisterUserCommand : ICommand
	{
		public RegisterUserCommand(string name, string password, string email)
		{
			UserName = name;
			Password = password;
			Email = email;
		}

		public string UserName { get; private set; }
		public string Password { get; private set; }
		public string Email { get; private set; }
	}
}