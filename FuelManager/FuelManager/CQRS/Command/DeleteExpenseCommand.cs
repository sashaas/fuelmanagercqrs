﻿using FuelManager.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FuelManager.CQRS.Command
{
	public class DeleteExpenseCommand : ICommand
	{
		public DeleteExpenseCommand(int id)
		{
			Id = id;
		}

		public int Id { get; private set; }
	}
}