﻿using FuelManager.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FuelManager.CQRS.Command
{
	public class AuthenticateUserCommand : ICommand
	{
		public AuthenticateUserCommand(string name, string pass)
		{
			Name = name;
			Password = pass;
		}

		public string Name { get; private set; }
		public string Password { get; private set; }
	}
}