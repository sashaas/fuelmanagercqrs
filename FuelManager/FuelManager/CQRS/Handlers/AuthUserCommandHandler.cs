﻿using FuelManager.Common;
using FuelManager.Core;
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace FuelManager.CQRS.Handlers
{
	public class AuthUserCommandHandler : ICommandHandler<CQRS.Command.AuthenticateUserCommand>
	{
		private readonly IUnitOfWorkFactory _fact;
		private readonly ICryptoService _crypto;

		public AuthUserCommandHandler(IUnitOfWorkFactory factory, ICryptoService crypto)
		{
			_fact = factory;
			_crypto = crypto;
		}

		public async Task<CommandResult> Execute(Command.AuthenticateUserCommand command)
		{
			IUnitOfWork uow = null;
			try
			{
				uow = _fact.Create(InstanceType.ReadOnly);
				string pass = await _crypto.Encrypt(command.Password, ConfigurationManager.AppSettings["Security:Phrase"]);
				IRepository<User> userRepo = uow.GetRepository<User>();
				var user = await userRepo.GetBy(x => x.Username == command.Name).FirstOrDefaultAsync();
				if (user == null)
					return CommandResult.CreateFailed(AuthResponse.CreateUserNotFound());

				if (!user.Password.Equals(pass, StringComparison.OrdinalIgnoreCase))
					return CommandResult.CreateFailed(AuthResponse.CreateInvalidPassword());
				return CommandResult.CreateOk(AuthResponse.CreateOk());
			}
			finally
			{
				if (uow != null)
					uow.Dispose();
			}
		}
	}
}