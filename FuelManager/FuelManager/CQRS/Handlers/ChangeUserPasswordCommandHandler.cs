﻿using FuelManager.Common;
using FuelManager.Core;
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace FuelManager.CQRS.Handlers
{
	public class ChangeUserPasswordCommandHandler : ICommandHandler<Command.ChangeUserPasswordCommand>
	{
		private readonly IUnitOfWorkFactory _factory;
		private readonly ICryptoService _crypto;

		public ChangeUserPasswordCommandHandler(IUnitOfWorkFactory factory, ICryptoService crypt)
		{
			_factory = factory;
			_crypto = crypt;
		}

		public async Task<CommandResult> Execute(Command.ChangeUserPasswordCommand command)
		{
			IUnitOfWork uow = null;
			try
			{
				uow = _factory.Create(InstanceType.Full);
				IRepository<User> userRepo = uow.GetRepository<User>();
				User user = await userRepo.GetBy(x => x.Username == command.UserName).FirstOrDefaultAsync();
				if (user == null)
					throw new InvalidOperationException("No such user has been found.");

				string oldCrypted = await _crypto.Encrypt(command.OldPassword, ConfigurationManager.AppSettings["Security:Phrase"]);
				string newCrypted = await _crypto.Encrypt(command.NewPassword, ConfigurationManager.AppSettings["Security:Phrase"]);
				if (user.Password != oldCrypted)
					throw new InvalidOperationException("Old password is not correct.");

				user.Password = newCrypted;
				userRepo.Update(user);

				await uow.SaveChanges();

				return new CommandResult() { Success = true, Message = "Password has been successfully changed." };
			}
			finally
			{
				if (uow != null)
					uow.Dispose();
			}
		}
	}
}