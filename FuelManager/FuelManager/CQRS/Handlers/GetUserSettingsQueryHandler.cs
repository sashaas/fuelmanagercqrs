﻿using FuelManager.Common;
using FuelManager.Core;
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace FuelManager.CQRS.Handlers
{
	public class GetUserSettingsQueryHandler : IQueryHandler<CQRS.Query.GetUserSettingsQuery, CQRS.Query.GetUserSettingsResult>
	{
		private readonly IUnitOfWorkFactory _fact;

		public GetUserSettingsQueryHandler(IUnitOfWorkFactory fact)
		{
			_fact = fact;
		}

		public async Task<Query.GetUserSettingsResult> Retrieve(Query.GetUserSettingsQuery query)
		{
			IUnitOfWork uow = null;
			try
			{
				uow = _fact.Create(InstanceType.ReadOnly);
				User user = await uow.GetRepository<User>().GetBy(x => x.Username == query.UserName).Include("Settings").FirstOrDefaultAsync();
				if (user == null)
					throw new InvalidOperationException("No such user has been found.");

				UserSettings settings = await uow.GetRepository<UserSettings>().GetBy(x => x.Id == user.Settings.Id).Include("SelctedCurrency").FirstOrDefaultAsync();
				if (settings == null)
					throw new InvalidOperationException("User has no settings defined.");
				return new CQRS.Query.GetUserSettingsResult(settings);
			}
			finally
			{
				if (uow != null)
					uow.Dispose();
			}
		}
	}
}