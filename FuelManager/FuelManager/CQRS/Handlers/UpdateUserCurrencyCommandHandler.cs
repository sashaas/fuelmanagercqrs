﻿using FuelManager.Common;
using FuelManager.Core;
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace FuelManager.CQRS.Handlers
{
	public class UpdateUserCurrencyCommandHandler : ICommandHandler<CQRS.Command.UpdateUserCurrencyCommand>
	{
		private readonly IUnitOfWorkFactory _factory;

		public UpdateUserCurrencyCommandHandler(IUnitOfWorkFactory fact)
		{
			_factory = fact;
		}

		public async Task<CommandResult> Execute(Command.UpdateUserCurrencyCommand command)
		{
			IUnitOfWork uow = null;

			try
			{
				uow = _factory.Create(InstanceType.Full);
				User user = await uow.GetRepository<User>().GetBy(x => x.Username == command.UserName).Include("Settings").FirstOrDefaultAsync();
				if (user == null)
					throw new InvalidOperationException("No such user has been found.");

				UserSettings settings = await uow.GetRepository<UserSettings>().GetBy(x => x.Id == user.Settings.Id).Include("SelctedCurrency").FirstOrDefaultAsync();
				if (settings == null)
					throw new InvalidOperationException("User has no settings defined.");

				CurrencyType newT = await uow.GetRepository<CurrencyType>().GetBy(x => x.Id == command.NewCurrencyId).FirstOrDefaultAsync();

				settings.CurrencyId = newT.Id;
				settings.SelctedCurrency = newT;
				uow.GetRepository<UserSettings>().Update(settings);

				await uow.SaveChanges();
				return new CommandResult() { Success = true, Message = "User currency has been updated." };
			}
			finally
			{
				if (uow != null)
					uow.Dispose();
			}
		}
	}
}