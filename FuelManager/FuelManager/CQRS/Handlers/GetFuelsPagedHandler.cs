﻿using FuelManager.Common;
using FuelManager.Controllers.Helpers;
using FuelManager.Core;
using FuelManager.Query;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace FuelManager.CQRS.Handlers
{
	public class GetFuelsPagedHandler : IQueryHandler<ShowPagedQuery, ShowPagedResult<FuelExpense>>
	{
		private readonly IUnitOfWorkFactory uowFact;
		public GetFuelsPagedHandler(IUnitOfWorkFactory fact)
		{
			if (fact == null)
				throw new InvalidOperationException("Factory instance cannot be a null.");

			uowFact = fact;
		}

		public async Task<ShowPagedResult<FuelExpense>> Retrieve(ShowPagedQuery query)
		{
			if (query == null)
				throw new InvalidOperationException("Query instance cannot be a null.");

			IUnitOfWork uow = null;
			try
			{
				uow = uowFact.Create(InstanceType.ReadOnly);
				var repo = uow.GetRepository<FuelExpense>();

				var request = query.TranslateToRequest();
				request.TotalCount = repo.Count;
				int totalPages = (int)Math.Ceiling((double)request.TotalCount / request.ItemsPerPage);
				request.TotalPages = totalPages;
				int skip = (request.Page - 1) * request.ItemsPerPage;
				var data = await repo.GetBy(x => x.Id > 0 && x.Owner.Id == query.OwnerId).Include("Owner")
				.ApplySort(query.SortCriteria).Skip(skip).Take(request.ItemsPerPage).ToListAsync();

				ShowPagedResult<FuelExpense> result = new ShowPagedResult<FuelExpense>(query.Page, data);
				return result;
			}
			finally
			{
				if (uow != null)
					uow.Dispose();
			}
		}
	}
}