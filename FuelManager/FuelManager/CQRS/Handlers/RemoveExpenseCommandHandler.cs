﻿using FuelManager.Common;
using FuelManager.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace FuelManager.CQRS.Handlers
{
	public class RemoveExpenseCommandHandler : ICommandHandler<CQRS.Command.DeleteExpenseCommand>
	{
		private readonly IUnitOfWorkFactory _factory;

		public RemoveExpenseCommandHandler(IUnitOfWorkFactory fact)
		{
			_factory = fact;
		}

		public async Task<CommandResult> Execute(Command.DeleteExpenseCommand command)
		{
			IUnitOfWork uow = null;
			try
			{
				uow = _factory.Create(InstanceType.Full);
				var repo = uow.GetRepository<FuelExpense>();
				FuelExpense expense = await repo.GetById(command.Id);
				if (expense != null)
				{
					repo.Delete(expense);
					await uow.SaveChanges();
					return new CommandResult() { Success = true };
				}
				else
					throw new InvalidOperationException("No such entity has been found.");
			}
			finally { if (uow != null) uow.Dispose(); }
		}
	}
}