﻿using FuelManager.Common;
using FuelManager.Core;
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace FuelManager.CQRS.Handlers
{
	public class GetUserRolesQueryHandler : IQueryHandler<Query.GetUserRolesQuery, Query.GetUserRolesResult>
	{
		private readonly IUnitOfWorkFactory _fact;
		public GetUserRolesQueryHandler(IUnitOfWorkFactory fact)
		{
			_fact = fact;
		}


		public async Task<Query.GetUserRolesResult> Retrieve(Query.GetUserRolesQuery query)
		{
			IUnitOfWork uow = null;
			try
			{
				uow = _fact.Create(InstanceType.ReadOnly);
				User user = await uow.GetRepository<User>().GetBy(x => x.Username == query.UserName).Include("Roles").FirstOrDefaultAsync();
				if (user == null)
					throw new InvalidOperationException("No such user has been found.");

				return new Query.GetUserRolesResult(user.Roles.Select(x => x.Name).ToArray());
			}
			finally
			{
				if (uow != null)
					uow.Dispose();
			}
		}
	}
}