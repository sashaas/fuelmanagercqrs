﻿using FuelManager.Common;
using FuelManager.Core;
using FuelManager.CQRS.Command;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace FuelManager.CQRS.Handlers
{
	public class CreateNewExpenseHandler : ICommandHandler<CreateNewExpenseCommand>
	{
		private readonly IUnitOfWorkFactory _fact;

		public CreateNewExpenseHandler(IUnitOfWorkFactory factory)
		{
			_fact = factory;
		}

		public async Task<CommandResult> Execute(CreateNewExpenseCommand command)
		{
			IUnitOfWork uow = null;
			CommandResult result = new CommandResult();
			try
			{
				FuelExpense expense = AutoMapper.Mapper.Map<FuelExpense>(command);
				uow = _fact.Create(InstanceType.Full);
				var repo = uow.GetRepository<FuelExpense>();
				var userRepo = uow.GetRepository<User>();
				var settingsRepo = uow.GetRepository<UserSettings>();

				var user = await userRepo.GetById(command.UserId);
				if (user == null)
					throw new InvalidOperationException("No such user was found.");

				expense.Owner = user;
				var setting = await settingsRepo.GetBy(x => x.Id == user.Id).Include(x => x.SelctedCurrency).FirstOrDefaultAsync();
				if (setting == null)
					throw new InvalidOperationException("User has not initialized settings.");
				expense.CurrencyTypeId = setting.CurrencyId;

				repo.Add(expense);
				await uow.SaveChanges();
				result.Success = true;
			}
			catch (System.Data.DBConcurrencyException ex)
			{
				GlobalShare.Instance.Logger.Error(ex.Message, ex);
				result.Success = false;
				throw;
			}
			finally
			{
				if (uow != null)
					uow.Dispose();
			}
			return result;
		}
	}
}