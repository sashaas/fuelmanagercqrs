﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FuelManager.CQRS.Query;
using FuelManager.Common;
using FuelManager.Core;
using System.Threading.Tasks;
namespace FuelManager.CQRS.Handlers
{
	public class GetFuelByIdHandler : IQueryHandler<GetByIdQuery, GetByIdResult<FuelExpense>>
	{
		private readonly IUnitOfWorkFactory uowFact;
		public GetFuelByIdHandler(IUnitOfWorkFactory fact)
		{
			if (fact == null)
				throw new InvalidOperationException("Factory instance cannot be a null.");

			uowFact = fact;
		}

		public async Task<GetByIdResult<FuelExpense>> Retrieve(GetByIdQuery query)
		{
			if (query == null)
				throw new InvalidOperationException("Query instance cannot be a null.");

			IUnitOfWork uow = null;
			try
			{
				uow = uowFact.Create(InstanceType.ReadOnly);
				var repo = uow.GetRepository<FuelExpense>();
				FuelExpense data = await repo.GetById(query.Id);
				GetByIdResult<FuelExpense> result = new GetByIdResult<FuelExpense>(data);
				return result;
			}
			finally
			{
				if (uow != null)
					uow.Dispose();
			}
		}
	}
}