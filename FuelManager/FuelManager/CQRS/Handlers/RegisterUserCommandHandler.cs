﻿using FuelManager.Common;
using FuelManager.Core;
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace FuelManager.CQRS.Handlers
{
	public class RegisterUserCommandHandler : ICommandHandler<Command.RegisterUserCommand>
	{
		IUnitOfWorkFactory _fact;
		ICryptoService _crypt;

		public RegisterUserCommandHandler(IUnitOfWorkFactory fact, ICryptoService crypt)
		{
			_fact = fact;
			_crypt = crypt;
		}

		public async Task<CommandResult> Execute(Command.RegisterUserCommand command)
		{
			if (command == null)
				throw new InvalidOperationException("Command instance cannot be a null");

			IUnitOfWork uow = null;
			try
			{
				uow = _fact.Create(InstanceType.Full);
				IRepository<User> userRepo = uow.GetRepository<User>();
				IRepository<UserInfo> userInfoRepo = uow.GetRepository<UserInfo>();
				IRepository<Role> roleRepo = uow.GetRepository<Role>();
				var user = await userRepo.GetBy(x => x.Username == command.UserName).FirstOrDefaultAsync();
				if (user != null)
					return new CommandResult { Success = false, Data = AuthResponse.CreateUserExists() };

				User usr = new User()
				{
					Username = command.UserName,
					Email = command.Email,
					UserInfo = new UserInfo { LastAccess = DateTime.Now }
				};
				usr.Password = await _crypt.Encrypt(command.Password, ConfigurationManager.AppSettings["Security:Phrase"]);

				userRepo.Add(usr);

				Role role = await roleRepo.GetBy(x => x.Name.Contains("System")).FirstOrDefaultAsync();
				if (role != null)
				{
					usr.Roles = new List<Role>() { role };
				}
				roleRepo.Update(role);
				await uow.SaveChanges();
				return new CommandResult { Success = true, Data = AuthResponse.CreateOk() };
			}
			finally
			{
				if (uow != null)
					uow.Dispose();
			}
		}
	}
}