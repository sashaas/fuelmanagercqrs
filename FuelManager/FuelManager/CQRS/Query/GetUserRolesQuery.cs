﻿using FuelManager.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FuelManager.CQRS.Query
{
	public class GetUserRolesQuery : IQuery
	{
		public GetUserRolesQuery(string username)
		{
			UserName = username;
		}
		public string UserName { get; private set; }
	}

	public class GetUserRolesResult : IQueryResult
	{
		public GetUserRolesResult(string[] roles)
		{
			Roles = roles;
		}

		public string[] Roles { get; private set; }
	}
}