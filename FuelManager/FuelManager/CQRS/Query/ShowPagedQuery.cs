﻿using FuelManager.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FuelManager.Query
{
	public class ShowPagedQuery : IQuery
	{
		public ShowPagedQuery(int count, int page, int ownerId)
		{
			Page = page;
			ItemsPerPage = count;
			OwnerId = ownerId;
		}

		public ShowPagedQuery(int count, int page, int ownerId, string sort)
			: this(count, page, ownerId)
		{
			SortCriteria = sort;
		}

		public int OwnerId { get; private set; }

		public string SortCriteria { get; private set; }

		public int Page { get; private set; }

		public int ItemsPerPage { get; private set; }

		public FuelManager.Common.PagedRequest TranslateToRequest()
		{
			return new FuelManager.Common.PagedRequest() { ItemsPerPage = this.ItemsPerPage, Page = this.Page };
		}
	}

	public class ShowPagedResult<T> : IQueryResult where T : class
	{
		public ShowPagedResult(List<T> data)
		{
			Result = data;
			Count = data.Count;
		}

		public ShowPagedResult(int page, List<T> data)
			: this(data)
		{
			PageNumber = page;
		}

		public List<T> Result { get; private set; }

		public int PageNumber { get; private set; }

		public int Count { get; private set; }
	}

}