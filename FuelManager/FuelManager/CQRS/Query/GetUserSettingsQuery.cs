﻿using FuelManager.Core;
using FuelManager.Common;
using FuelManager.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FuelManager.CQRS.Query
{
	public class GetUserSettingsQuery : IQuery
	{
		public GetUserSettingsQuery(string userName)
		{
			UserName = userName;
		}

		public string UserName { get; set; }
	}

	public class GetUserSettingsResult : IQueryResult
	{
		public GetUserSettingsResult(UserSettings set)
		{
			if (set == null)
				throw new InvalidOperationException("Currency cannot be a null.");

			Settings = set;
		}

		public UserSettings Settings { get; private set; }
	}
}