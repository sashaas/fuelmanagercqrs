﻿using FuelManager.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FuelManager.CQRS.Query
{
	public class GetByIdQuery : IQuery
	{
		public GetByIdQuery(int id)
		{
			Id = id;
		}

		public int Id { get; private set; }
	}

	public class GetByIdResult<T> : IQueryResult
	{
		public GetByIdResult(T data)
		{
			Data = data;
		}

		public T Data { get; private set; }
	}
}