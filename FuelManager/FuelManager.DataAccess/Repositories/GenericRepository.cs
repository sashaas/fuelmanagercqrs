﻿using FuelManager.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelManager.DataAccess.Repositories
{
	public class GenericRepository<T> : IRepository<T> where T : EntityBase
	{
		private readonly DbContext _ctx;

		public GenericRepository(DbContext context)
		{
			_ctx = context;
		}

		public int Count
		{
			get { return _ctx.Set<T>().Count(); }
		}

		public IEnumerable<T> GetAll()
		{
			return _ctx.Set<T>().AsEnumerable();
		}

		public Task<List<T>> GetAllAsync()
		{
			return _ctx.Set<T>().ToListAsync();
		}

		public IEnumerable<T> GetPaged(PagedRequest request)
		{
			int skipIndex = request.Page * request.ItemsPerPage;

			return _ctx.Set<T>().OrderBy(x => x.Id).Skip(skipIndex).Take(request.ItemsPerPage).AsEnumerable();
		}

		public async Task<List<T>> GetPagedAsync(PagedRequest request)
		{
			int skipIndex = request.Page * request.ItemsPerPage;

			return await _ctx.Set<T>().OrderBy(x => x.Id).Skip(skipIndex).Take(request.ItemsPerPage).ToListAsync() ;
		}

		public IQueryable<T> GetBy(System.Linq.Expressions.Expression<Func<T, bool>> exp)
		{
			return _ctx.Set<T>().Where(exp);
		}

		public async Task<T> GetById(int id)
		{
			return await _ctx.Set<T>().FirstOrDefaultAsync();
		}

		public void Add(T entity)
		{
			_ctx.Set<T>().Add(entity);
		}

		public void Update(T entity)
		{
			if (!_ctx.Set<T>().Local.Any(x => x.Id == entity.Id))
				_ctx.Set<T>().Attach(entity);

			if (_ctx.Entry<T>(entity).State == System.Data.Entity.EntityState.Unchanged)
				_ctx.Entry<T>(entity).State = System.Data.Entity.EntityState.Modified;
		}

		public void Delete(T entity)
		{
			_ctx.Set<T>().Remove(entity);
		}


	}
}
