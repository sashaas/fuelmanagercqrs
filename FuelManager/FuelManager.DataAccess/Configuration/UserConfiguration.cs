﻿using FuelManager.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelManager.DataAccess.Configuration
{
	internal class UserConfiguration : EntityTypeConfiguration<User>
	{
		public UserConfiguration()
		{
			ToTable("Users");
			HasKey(x => x.Id);
			Property(x => x.Id).HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);

			Property(x => x.Username).IsRequired();
			Property(x => x.Password).IsRequired();
			Property(x => x.Email).IsRequired();

			this.HasMany(u => u.FuelExpenses).WithRequired(f => f.Owner)
			.Map(p => p.MapKey("UserId")).WillCascadeOnDelete();
		}
	}
}
