﻿using FuelManager.Common;
using System;
using System.Collections.Concurrent;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Text;
using System.Threading.Tasks;

namespace FuelManager.DataAccess
{
	public class UnitOfWork : IUnitOfWork
	{
		private readonly DbContext _ctx;
		private readonly ConcurrentDictionary<Type, object> _repos;
		private readonly log4net.ILog _logger;

		public UnitOfWork(DbContext ctx, log4net.ILog log)
		{
			_ctx = ctx;
			_repos = new ConcurrentDictionary<Type, object>();
			_logger = log;
		}

		public System.Data.Entity.DbContext Context
		{
			get { return _ctx; }
		}

		public IRepository<T> GetRepository<T>() where T : EntityBase
		{
			Type requesteType = typeof(T);

			object instance;
			if (_repos.TryGetValue(requesteType, out instance))
				return instance as IRepository<T>;

			IRepository<T> newRepo = new Repositories.GenericRepository<T>(_ctx);
			_repos.TryAdd(requesteType, newRepo);
			return newRepo;
		}

		public async Task<int> SaveChanges()
		{
			try
			{
				int processed = await _ctx.SaveChangesAsync();
				if (_logger.IsDebugEnabled)
					_logger.Debug(string.Format("SaveChangesAsync processed {0} items", processed));
				return processed;
			}
			catch (DbEntityValidationException ex)
			{
				StringBuilder errorBuilder = new StringBuilder();

				foreach (var validErrors in ex.EntityValidationErrors)
				{
					foreach (var error in validErrors.ValidationErrors)
					{
						errorBuilder.AppendLine(string.Format("Property: {0}, Error: {1}", error.PropertyName, error.ErrorMessage));
					}
				}

				var dbExc = new ApplicationException(errorBuilder.ToString(), ex);
				if (_logger.IsErrorEnabled)
					_logger.Error("SaveChanges", dbExc);
				throw dbExc;
			}
		}

		public void Dispose()
		{
			_ctx.Dispose();
			_repos.Clear();
		}
	}
}
