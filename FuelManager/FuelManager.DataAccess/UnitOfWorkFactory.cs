﻿using FuelManager.Common;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelManager.DataAccess
{
	public class UnitOfWorkFactory : IUnitOfWorkFactory
	{
		private readonly log4net.ILog _logger;

		public UnitOfWorkFactory(ILog logger)
		{
			_logger = logger;
		}

		public IUnitOfWork Create(InstanceType type)
		{
			FuelDbContext ctx = new FuelDbContext("FuelCtx");

			if (type == InstanceType.ReadOnly)
			{
				ctx.Configuration.AutoDetectChangesEnabled = false;
				ctx.Configuration.LazyLoadingEnabled = false;
				ctx.Configuration.ProxyCreationEnabled = false;
			}

			return new UnitOfWork(ctx, _logger);
		}
	}
}
