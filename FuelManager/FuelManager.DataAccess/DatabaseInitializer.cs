﻿using FuelManager.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FuelManager.DataAccess
{
	public class DatabaseInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<FuelDbContext>
	{
		protected override void Seed(FuelDbContext context)
		{
			base.Seed(context);

			if (context.Roles.Count() == 0)
			{
				var roles = new List<Role>{
										new Role(){ Name="Admin", Description= "User has an access to all system components"},
										new Role(){ Name="User", Description= "System authorized user"}
									};

				roles.ForEach(r => context.Roles.Add(r));

				context.SaveChanges();
			}

			if (context.Users.FirstOrDefault(x => x.Username == "admin") == null)
			{
				Role adminRole = context.Roles.FirstOrDefault(x => x.Name == "Admin");

				User usr = new User()
				{
					Username = "admin",
					Password = FuelManager.Common.Utils.PasswordHash.CreateHash("admin"),
					Email = "testuser@mail.ru",
					Roles = new List<Role>() { adminRole }
				};

				context.Users.Add(usr);
				context.SaveChanges();
			}
		}
	}
}
