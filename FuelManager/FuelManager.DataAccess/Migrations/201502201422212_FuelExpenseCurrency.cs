namespace FuelManager.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FuelExpenseCurrency : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FuelExpense",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Litres = c.Double(nullable: false),
                        PricePerLiter = c.Double(nullable: false),
                        OverallPrice = c.Double(nullable: false),
                        GasStation = c.String(),
                        MileageBeforeRefuel = c.Double(nullable: false),
                        CurrencyTypeId = c.Int(nullable: false),
                        IP = c.String(),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CurrencyType", t => t.CurrencyTypeId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.CurrencyTypeId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.CurrencyType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Country = c.String(),
                        Description = c.String(),
                        IP = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FuelExpense", "UserId", "dbo.Users");
            DropForeignKey("dbo.FuelExpense", "CurrencyTypeId", "dbo.CurrencyType");
            DropIndex("dbo.FuelExpense", new[] { "UserId" });
            DropIndex("dbo.FuelExpense", new[] { "CurrencyTypeId" });
            DropTable("dbo.CurrencyType");
            DropTable("dbo.FuelExpense");
        }
    }
}
