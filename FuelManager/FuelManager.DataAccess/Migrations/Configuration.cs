namespace FuelManager.DataAccess.Migrations
{
	using FuelManager.Common;
	using System;
	using System.Collections.Generic;
	using System.Data.Entity;
	using System.Data.Entity.Migrations;
	using System.Linq;

	internal sealed class Configuration : DbMigrationsConfiguration<FuelManager.DataAccess.FuelDbContext>
	{
		public Configuration()
		{
			AutomaticMigrationsEnabled = false;
		}

		protected override void Seed(FuelManager.DataAccess.FuelDbContext context)
		{
			List<Role> roles = new List<Role>(){

					new Role(){ Name="Admin", Description="User with administrator priviliges"},
					new Role{ Name="SystemUser", Description="User which has a system access priviledges for hsi data"}
				};

			context.Roles.AddRange(roles);
			context.SaveChanges();

			List<User> users = new List<User>(){
					new User(){ Username="Admin", Password="cGO0LgXSmEosuVs5jI/59A==", Email="sashaas@inbox.ru", UserInfo=new UserInfo(){ FirstName="Oleksandr", LastAccess=DateTime.Now}}
				};

			users.ForEach(u => context.Users.AddOrUpdate(u));
			context.SaveChanges();

			Role role = context.Roles.FirstOrDefault(x => x.Name == "Admin");
			User user = context.Users.FirstOrDefault(x => x.Username == "Admin");

			if (role.Users == null)
				role.Users = new List<User>();

			role.Users.Add(user);

			context.Roles.AddOrUpdate(role);
			context.SaveChanges();
		}
	}
}
