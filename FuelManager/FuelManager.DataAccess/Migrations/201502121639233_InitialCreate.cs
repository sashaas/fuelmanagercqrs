namespace FuelManager.DataAccess.Migrations
{
	using System;
	using System.Data.Entity.Migrations;

	public partial class InitialCreate : DbMigration
	{
		public override void Up()
		{
			CreateTable(
				"dbo.Roles",
				c => new
					{
						Id = c.Int(nullable: false, identity: true),
						Name = c.String(nullable: false, maxLength: 30),
						Description = c.String(),
						IP = c.String(),
					})
				.PrimaryKey(t => t.Id);

			CreateTable(
				"dbo.Users",
				c => new
					{
						Id = c.Int(nullable: false, identity: true),
						Username = c.String(nullable: false),
						Email = c.String(nullable: false),
						Password = c.String(nullable: false),
						IP = c.String(),
					})
				.PrimaryKey(t => t.Id);

			CreateTable(
				"dbo.UserInfos",
				c => new
					{
						Id = c.Int(nullable: false),
						FirstName = c.String(),
						LastName = c.String(),
						Address = c.String(),
						LastAccess = c.DateTime(),
						IP = c.String(),
					})
				.PrimaryKey(t => t.Id)
				.ForeignKey("dbo.Users", t => t.Id)
				.Index(t => t.Id);

			CreateTable(
				"dbo.UserRoles",
				c => new
					{
						RoleId = c.Int(nullable: false),
						UserId = c.Int(nullable: false),
					})
				.PrimaryKey(t => new { t.RoleId, t.UserId })
				.ForeignKey("dbo.Roles", t => t.RoleId, cascadeDelete: true)
				.ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
				.Index(t => t.RoleId)
				.Index(t => t.UserId);

		}

		public override void Down()
		{
			DropForeignKey("dbo.UserRoles", "UserId", "dbo.Users");
			DropForeignKey("dbo.UserRoles", "RoleId", "dbo.Roles");
			DropForeignKey("dbo.UserInfos", "Id", "dbo.Users");
			DropIndex("dbo.UserRoles", new[] { "UserId" });
			DropIndex("dbo.UserRoles", new[] { "RoleId" });
			DropIndex("dbo.UserInfos", new[] { "Id" });
			DropTable("dbo.UserRoles");
			DropTable("dbo.UserInfos");
			DropTable("dbo.Users");
			DropTable("dbo.Roles");
		}
	}
}
