namespace FuelManager.DataAccess.Migrations
{
	using System;
	using System.Data.Entity.Migrations;

	public partial class UserSettings : DbMigration
	{
		public override void Up()
		{
			CreateTable(
				"dbo.UserSettings",
				c => new
					{
						Id = c.Int(nullable: false),
						CurrencyId = c.Int(nullable: false),
						IP = c.String(),
					})
				.PrimaryKey(t => t.Id)
				.ForeignKey("dbo.Users", t => t.Id)
				.ForeignKey("dbo.CurrencyType", t => t.CurrencyId, cascadeDelete: true)
				.Index(t => t.Id)
				.Index(t => t.CurrencyId);

			base.Sql(@"INSERT INTO CurrencyType(Name,Country,Description) values('UAH','Ukraine','');
					INSERT INTO CurrencyType(Name,Country,Description) values('USD','United States','');
					INSERT INTO CurrencyType(Name,Country,Description) values('EUR','European','');");

		}

		public override void Down()
		{
			DropForeignKey("dbo.UserSettings", "CurrencyId", "dbo.CurrencyType");
			DropForeignKey("dbo.UserSettings", "Id", "dbo.Users");
			DropIndex("dbo.UserSettings", new[] { "CurrencyId" });
			DropIndex("dbo.UserSettings", new[] { "Id" });
			DropTable("dbo.UserSettings");
		}
	}
}
